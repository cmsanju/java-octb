package com.test;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		PrintWriter out = response.getWriter();
		
		ServletContext ctx = getServletContext();
		
		String c1 = ctx.getInitParameter("city1");
		String c2 = ctx.getInitParameter("city2");
		
		String dt = c1+" "+c2;
		
		out.println("from servlet1 : "+dt);
		
		ctx.setAttribute("info", dt);
		
		out.println("<a href = 'Servlet2'> go to next page</a> ");
		
		
		ServletConfig cfg = getServletConfig();
		
		String usr = cfg.getInitParameter("user");
		String pas = cfg.getInitParameter("pwd");
		
		out.println("from servlet 1 : config data "+usr+" "+pas);
		
		
	}

}
